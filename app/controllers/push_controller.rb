class PushController < ApplicationController
  before_action :read_config, :get_params
  def index
  end

  def format
    @warning = ''
    @us_detail = {}
    @hide_header = true
    @files = []
    @userstories.each_with_index do |userstory, idx|
      api_url = @jira_api_uri + '/issue/' + userstory['id']
      response = HTTParty.get(api_url, :headers => {"Authorization" => @jira_auth})

      if response.code == 200 then
        userstory['title'] = response['fields']['summary']
        userstory['url'] = 'https://msts-eng.atlassian.net/browse/' + userstory['id']
        @userstories[idx] = userstory
      else
        @userstories.delete_at(idx)
      end

      url = @gitlab_api_uri + '/merge_requests?scope=all&source_branch=' + userstory['id']
      response = HTTParty.get(url, :headers => @gitlab_auth)

      if response.code == 200 then
        @response = JSON.parse(response.body)
        @response.each do | task |
          project = task['project_id']
          request = task['iid']
          url = @gitlab_api_uri + "/projects/#{project}/merge_requests/#{request}/changes"
          response = HTTParty.get(url, :headers => @gitlab_auth)
          if response.code == 200 then
            @response = JSON.parse(response.body)
            if @response['merge_commit_sha'].nil?
              sha1 = 'XXXXXX'
            else
              sha1 = @response['merge_commit_sha'].to_s[0, 8]
            end
            branch = @response['source_branch']
            @response['changes'].each do |change|
              if change['deleted_file']
                action = 'deleted'
              elsif change['renamed_file']
                action = 'renamed'
              elsif change['new_file']
                action = 'new'
              else
                action = 'modified'
              end
              file = change['old_path']
              if change['old_path'] != change['new_path']
                file += ' => ' + change['new_path']
              end
              @files.push "#{action}: #{file} | #{sha1} | #{branch}"
            end
          end
        end
      end
    end
  end

  def get_params
    @userstories = []
    params[:userstories].to_s.split(/ /).each do |id|
      @userstories.push ({"id"=>id})
    end
  end
end
