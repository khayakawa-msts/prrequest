require 'base64'

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  def read_config
    @jira_api_uri = 'https://msts-eng.atlassian.net/rest/api/latest'
    @jira_uri = 'https://msts-eng.atlassian.net/browse'
    @gitlab_api_uri = 'https://scm.multiservice.com/gitlab/api/v4'

    f = File.open( ENV['HOME'] + '/.jira', 'r')
    file = f.each_line.map { |x| x.strip }
    f.close
    # @jira_auth = {:username => file[0], :password => file[1]}
    @jira_auth = 'Basic ' + Base64.encode64(file[0])

    f = File.open( ENV['HOME'] + '/.gitlab', 'r')
    file = f.each_line.map { |x| x.strip }
    f.close
    @gitlab_auth = {"private-token" => file[0]}
  end
end
