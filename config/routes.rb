Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/' => 'prr#index'
  get 'prr/format' => 'prr#format'
  get 'push' => 'push#index'
  get 'push/format' => 'push#format'
end
